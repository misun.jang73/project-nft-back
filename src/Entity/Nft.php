<?php

namespace App\Entity;

use App\Repository\NftRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: NftRepository::class)]
class Nft
{
    #[Groups(["api"])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(["api"])]
    #[ORM\Column(length: 255)]
    private ?string $nameNft = null;

    #[Groups(["api"])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[Groups(["api"])]
    #[ORM\Column(length: 255)]
    private ?string $pathURL = null;

    #[Groups(["api"])]
    #[ORM\Column]
    private ?int $priceEth = null;

    
    #[Groups(["api"])]
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: "nft" , cascade:["persist"])]
    private Collection $user;

    #[ORM\ManyToMany(targetEntity: SubCategory::class, inversedBy: 'nfts')]
    private Collection $subCategories;

    #[ORM\ManyToOne(inversedBy: 'nfts')]
    private ?User $userNft = null;

    public function __construct()
    {
        $this->subCategories = new ArrayCollection();
        // $this->user = new ArrayCollection(); //a ajouté
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameNft(): ?string
    {
        return $this->nameNft;
    }

    public function setNameNft(string $nameNft): static
    {
        $this->nameNft = $nameNft;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPathURL(): ?string
    {
        return $this->pathURL;
    }

    public function setPathURL(string $pathURL): static
    {
        $this->pathURL = $pathURL;

        return $this;
    }

    public function getPriceEth(): ?int
    {
        return $this->priceEth;
    }

    public function setPriceEth(int $priceEth): static
    {
        $this->priceEth = $priceEth;

        return $this;
    }

    public function getUser(): Collection
    {
        return $this->user;
    }

    // public function setUser(?User $user): static
    // {
    //     $this->user = $user;

    //     return $this;
    // }

    public function setUser(User $user): static
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    /**
     * @return Collection<int, SubCategory>
     */
    public function getSubCategories(): Collection
    {
        return $this->subCategories;
    }

    public function addSubCategory(SubCategory $subCategory): static
    {
        if (!$this->subCategories->contains($subCategory)) {
            $this->subCategories->add($subCategory);
        }

        return $this;
    }

    public function removeSubCategory(SubCategory $subCategory): static
    {
        $this->subCategories->removeElement($subCategory);

        return $this;
    }

    public function __toString(): string
    {
        return $this->nameNft ?? '';
    }

    public function getUserNft(): ?User
    {
        return $this->userNft;
    }

    public function setUserNft(?User $userNft): static
    {
        $this->userNft = $userNft;

        return $this;
    }
    
    
}
