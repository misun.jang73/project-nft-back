<?php

namespace App\Controller;

use App\Entity\Eth;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EthController extends AbstractController
{
    #[Route('/eth', name: 'app_eth')]
    public function index(HttpClientInterface $http , EntityManagerInterface $em): Response
    {
        $response = $http->request("GET" ,"https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=EUR");
        $response = json_decode($response->getContent(), true);
        $value = $response["EUR"];
        $date = new \DateTime();

        $Eth = new Eth();
        $Eth->setValueEth($value);
        $Eth->setCreatedAt($date);
        
        $em->persist($Eth);
        $em->flush();

        return $this->render('eth/index.html.twig', [
            'controller_name' => 'EthController',
        ]);
    }
}
