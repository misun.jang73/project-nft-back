<?php

namespace App\Controller\Api;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Annotation\Groups;


#[Route('/api')]
class ApiCategoryController extends AbstractController
{
    public function __construct(
        private CategoryRepository $categoryRepository
    ) {
    }

    #[Route('/category', name: 'app_api_category')]
    public function indexApiCategory(SerializerInterface $serializer): Response
    {
        $categories = $this->categoryRepository->findAll();

        $jsonContent = $serializer->serialize($categories, 'json', ['groups' => ['api', 'subCategory_relation']]);
        return new JsonResponse($jsonContent, 200, [], true);
            }

    #[Route('/new', name: 'api_category_new', methods: ['POST'])]
    public function apiCategoryNew(EntityManagerInterface $entityManager, Request $request)    {
        $requestData = json_decode($request->getContent(), true);

        $categroy = new Category();
        $categroy->setNameCategory($requestData['nameCategory']);
        $entityManager->persist($categroy);
        $entityManager->flush();

        return new JsonResponse($categroy, Response::HTTP_CREATED, [], true);
    }

    #[Route('/categroy/{id}', name: 'api_categroy_id', methods: ['GET'])]
    public function apiCategroyId(int $id): Response
    {
        $categroy = $this->categoryRepository->find($id);

        if (!$categroy) {
            return new JsonResponse(['error' => 'Categroy not found.'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($categroy, 200, ['groups' => 'api']);
    }
}
