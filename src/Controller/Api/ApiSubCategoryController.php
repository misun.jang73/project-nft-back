<?php

namespace App\Controller\Api;

use App\Entity\SubCategory;
use App\Repository\SubCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class ApiSubCategoryController extends AbstractController
{
    public function __construct(
        private SubCategoryRepository $subCategoryRepository
    ) {
    }

    #[Route('/sub_category', name: 'app_api_subCategory')]
    public function indexApiSubCategory(): Response
    {
        $subCategories = $this->subCategoryRepository->findAll();

        return $this->json($subCategories, 200, ['groups' => ['api']]);
    }

    #[Route('/new', name: 'api_subCategory_new', methods: ['POST'])]
    public function apiSubCategoryNew(EntityManagerInterface $entityManager, Request $request)
    {
        $requestData = json_decode($request->getContent(), true);

        $subCategory = new SubCategory();

        $subCategory->setNameSubCategory($requestData['nameSubCategory']);

        $entityManager->persist($subCategory);
        $entityManager->flush();

        return new JsonResponse($subCategory, Response::HTTP_CREATED, [], true);
    }

    #[Route('/sub_category/{id}', name: 'api_subCategory_id', methods: ['GET'])]
    public function apiSubCategoryById(int $id): Response
    {
        $subCategory = $this->subCategoryRepository->find($id);

        if (!$subCategory) {
            return new JsonResponse(['error' => 'SubCategory not found.'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($subCategory, 200, ['groups' => 'api']);
    }
}
