<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiTestController extends AbstractController
{
    #[Route('/api/test', name: 'app_api_test')]
    public function index(): Response
    {
        if($this->getUser() === null){
            return $this->json("pas connecté");
        }else{
            // return $this->json("connecté tant que " . $this->getUser()->getEmail());
            return $this->json("connecté tant que " . $this->getUser()->email);
        }
    }
}
