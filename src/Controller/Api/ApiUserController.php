<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


#[Route('/api')]
class ApiUserController extends AbstractController
{
    public function __construct(
        private UserRepository $userRepository
    ) {}

    #[Route('/user', name: 'app_api_user')]
    public function indexApiUser(): Response
    {
        $users = $this->userRepository->findAll();

        return $this->json($users, 200, ['groups' => ['api']]);
    }

    #[Route('/new', name: 'api_user_new', methods: ['POST'])]
    public function apiUserNew(EntityManagerInterface $entityManager, Request $request)
    {
        $requestData = json_decode($request->getContent(), true);

        $user = new User();

        $user->setFirstName($requestData['firstName']);
        $user->setLastName($requestData['lastName']);
        $user->setEmail($requestData['email']);
        $user->setRoles($requestData['roles']);
        $user->setNumberAddress($requestData['numberAddress']);
        $user->setNameAddress($requestData['nameAddress']);
        $user->setComplementAddress($requestData['complementAddress']);
        $user->setCity($requestData['city']);

        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse($user, Response::HTTP_CREATED, [], true);
    }

    #[Route('/user/{id}', name: 'api_user_id', methods: ['GET'])]
    public function apiUserId(int $id): Response
    {
        $user = $this->userRepository->find($id);

        if (!$user) {
            return new JsonResponse(['error' => 'User not found.'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($user, 200, ['groups' => 'api']);
    }
}
