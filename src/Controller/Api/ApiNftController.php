<?php

namespace App\Controller\Api;

use App\Entity\Nft;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\NftRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/api')]
class ApiNftController extends AbstractController
{
    public function __construct(
        private NftRepository $nftRepository,
        private UserRepository $userRepository
    ) {
    }

    #[Route('/nft', name: 'app_nft_api')]
    public function indexApiNft(): Response
    {
        $nft = $this->nftRepository->findAll();
        
        return $this->json($nft, 200, ['groups' => ['api', 'nft_relation']]);
    }

    #[Route('/nft/new', name: 'api_nft_new', methods: ['POST'])]
    public function apiNftNew(EntityManagerInterface $entityManager, Request $request)
    {

        $requestData = json_decode($request->getContent(), true);
    
        $nft = new Nft();
        
        $nft->setNameNft($requestData['nameNft']);
        $nft->setDescription($requestData['description']);
        $nft->setPathUrl($requestData['pathURL']);
        $nft->setPriceEth($requestData['priceEth']);
        

        // $nft->setFilePath($requestData['filePath']);
        // $nft->setAlt($requestData['alt']);
    
        $entityManager->persist($nft);
        $entityManager->flush();
    
        return new JsonResponse($nft, Response::HTTP_CREATED, [], true);
    }
    
    #[Route('/nft/{id}', name: 'api_nft_id')]
    public function apiNftId(int $id): Response
    {
        $nft = $this->nftRepository->find($id);
        return $this->json($nft, 200, ['groups' => 'api']);
    }
}
