<?php

namespace App\Controller;

use App\Entity\SubCategory;
use App\Repository\SubCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\SubCategorySearchType;
use App\Form\SubCategoryType;

#[Route('/subCategory')] 
class SubCategoryController extends AbstractController
{
    public function __construct(
        private SubCategoryRepository $subCategoryRepository,
        private EntityManagerInterface $entityManager,
        private PaginatorInterface $paginator
    )
    {
        
    }

    #[Route('/', name: 'app_sub_category_index')]
    public function index(Request $request): Response
    {
        $qb = $this->subCategoryRepository->getQball();
        $form = $this->createForm(SubCategorySearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if ($data['nameSubCategory'] !== null) {
                $qb->where('c.nameSubCategory LIKE :nameSubCategory')
                    ->setParameter('nameSubCategory', "%" . $data['nameSubCategory'] . "%");
            }
        }

        $pagination = $this->paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            15
        );

        return $this->render('sub_category/index.html.twig', [
            'subCategories' => $pagination, 
            'form' => $form->createView()
        ]);
    }

    #[Route('/add', name: 'app_sub_category_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $subCategory = new SubCategory();
        $form = $this->createForm(SubCategoryType::class, $subCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            
            $entityManager->persist($subCategory);
            $entityManager->flush();

            return $this->redirectToRoute('app_sub_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('sub_category/new.html.twig', [
            'subCategory' => $subCategory,
            'formSubCategory' => $form->createView()
        ]);
    }

    #[Route('/show/{id}', name: 'app_sub_category_show')]
    public function showSubCategory($id): Response
    {
        $subCategoryEntity = $this->subCategoryRepository->find($id);

        if ($subCategoryEntity === null) {
            return $this->redirectToRoute('app_home');
        }
        return $this->render('sub_category/show.html.twig', [
            'subCategory' => $subCategoryEntity,
        ]);
    }

    #[Route('/update/{id}', name: 'app_sub_category_edit')]
    public function update(Request $request, $id): Response
    {
        $subCategory = $this->subCategoryRepository->find($id);

        $form = $this->createForm(SubCategoryType::class, $subCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($subCategory); 
            $this->entityManager->flush();

            return $this->redirectToRoute('app_sub_category_index');
        }

        return $this->render('sub_category/edit.html.twig', [
            'form' => $form->createView(),
            'subCategory' => $subCategory
        ]);
    }

    #[Route('/delete/{id}', name: 'app_sub_category_delete')]
    public function delete($id): Response
    {
        $subCategory = $this->subCategoryRepository->find($id);
        
        if ($subCategory !== null) {
            $this->entityManager->remove($subCategory);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app_sub_category_index');
    }
}
