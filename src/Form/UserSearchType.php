<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('firstName')
        ->add('lastName')
        ->add('email')
        ->add('city')
        ->add('numberAddress')
        ->add('nameAddress')
        ->add('complementAddress')

        // ->setMethod('GET')
        // ->add('firstName', TextType::class, [
        //     'label' => "Nom de l'utilisateur",
        //     'required' => false
        //     ])
    
    ;
}

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}