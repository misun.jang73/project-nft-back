<?php

namespace App\Form;

use App\Entity\SubCategory;
use App\Entity\Category;
use App\Entity\Nft;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SubCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nameSubCategory')
            ->add('Nfts', EntityType::class, [
                "class" => Nft::class,
                "multiple" => true,
            ])
            ->add('category', EntityType::class, [
                "class" => Category::class,
                "multiple" => false,
                "choice_label"=>"nameCategory"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SubCategory::class,
        ]);
    }
}
