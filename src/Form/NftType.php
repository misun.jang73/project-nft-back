<?php

namespace App\Form;

use App\Entity\Nft;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\SubCategory;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;

class NftType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nameNft')
            ->add('description')
            ->add('priceEth')
            // ->add('user', EntityType::class, [
            //     'class' => User::class,
            //     'multiple' => true, // Assuming user is a ManyToMany relationship
            //     'choice_label' => 'username', // Replace with the actual property you want to display
            //     'expanded' => true, // You can change this based on your needs
            //     'required' => true, // Set to true if you want to require at least one user
            //     'constraints' => [
            //         new Count(['min' => 1, 'minMessage' => 'Select at least one user']), // Add this constraint to require at least one user
            //     ],
            // ])
            // ->add('subCategories', EntityType::class, [
            //     'class' => SubCategory::class,
            //     'multiple' => true, // Assuming subCategories is a ManyToMany relationship
            //     'choice_label' => 'name', // Replace with the actual property you want to display
            //     'expanded' => true, // You can change this based on your needs
            //     'required' => false, // Set to true if you want to require at least one subCategory
            // ])
            ->add('subCategories', EntityType::class , [
                "class" => SubCategory::class,
                "multiple" => true,
            ])
            ->add("file",FileType::class, [
                "mapped"=>false,
                "required" => true
            ])
            ->add('user', EntityType::class , [
                "class" => User::class,
                "multiple" => false,
                "constraints" => [
                    new NotBlank(["message" =>"select a user"
                    ])
                ]
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Nft::class,
        ]);
    }
}
